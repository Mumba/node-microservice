/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {Subject} from 'rxjs/Subject';
import {Router} from 'mumba-microservice-router';

import {Action, BaseMicroservice, BaseMicroserviceOptions} from '../../src/index';
import {assertRejects} from '../bootstrap';

describe('BaseMicroservice unit tests', () => {
	let options: BaseMicroserviceOptions;
	let actionStub: Action;
	let routerStub: Router;
	let rejectingRouterStub: Router;
	let instance: BaseMicroservice;

	beforeEach(() => {
		options = {};
		actionStub = <Action>{
			getName: () => 'foo.bar',
			invoke: () => Promise.resolve()
		};

		routerStub = <Router><any>{
			onRegister: new Subject(),
			onSubscribe: new Subject(),
			register: (name: string, callback: any) => {
				routerStub.onRegister.next({ procedure: name, callback });
			},
			subscribe: (topic: string, callback: any) => {
				routerStub.onSubscribe.next({ topic, callback });
			}
		};

		rejectingRouterStub = <Router><any>{
			onRegister: new Subject(),
			onSubscribe: new Subject(),
			register: (name: string, callback: any) => {
				rejectingRouterStub.onRegister.error(new Error('reg-error'));
			},
			subscribe: (topic: string, callback: any) => {
				rejectingRouterStub.onSubscribe.error(new Error('sub-error'));
			}
		};

		instance = new BaseMicroservice(options, routerStub);
	});

	it('should throw if `router` is missing', () => {
		assert.throws(() => {
			instance = new BaseMicroservice(options, void 0);
		}, /<router> required/);
	});

	it('should set the router', () => {
		const router = <Router>{};

		assert.strictEqual(instance.setRouter(router), instance, 'should chain');
	});

	it('should register the action and publish an event', (done) => {
		instance.onRegister.subscribe((name: string) => {
			try {
				assert.equal(name, 'foo.bar', 'should be the name of the action registered');
				done();
			}
			catch (e) {
				done();
			}
		});

		instance.registerAction(actionStub);
	});

	it('should catch an error when registering an action', (done) => {
		instance = new BaseMicroservice(options, rejectingRouterStub);

		instance.onError.subscribe((err: Error) => {
			try {
				assert.equal(err.message, 'reg-error', 'should be the error');
				done();
			}
			catch (e) {
				done();
			}
		});

		instance.registerAction(actionStub);
	});

	it('should subscribe an action to a topic on the router and publish an event', (done) => {
		instance.onSubscribe.subscribe((name: string) => {
			assert.equal(name, 'foo.bar', 'should be the name of the action registered');
			done();
		});

		instance.subscribeAction(actionStub);
	});

	it('should catch an error when subscribing an action', (done) => {
		instance = new BaseMicroservice(options, rejectingRouterStub);

		instance.onError.subscribe((err: Error) => {
			try {
				assert.equal(err.message, 'sub-error', 'should be the error');
				done();
			}
			catch (e) {
				done();
			}
		});

		instance.subscribeAction(actionStub);
	});

	it('should publish an event when an action is invoked', (done) => {
		// Need to reach into the router stub to be able to invoke the callback.
		// This is a code smell. The way around it is probably to mount observers on the actions themselves?
		routerStub.onRegister.subscribe((details: any) => {
			// Invoke the callback
			details.callback(0, [101], { foo: 'bar' });
		});

		instance.onInvoke.subscribe((details: any) => {
			try {
				assert.equal(details.action, 'foo.bar', 'should be the name of the action');
				assert.equal(details.authid, 0, 'should add the authid to the details');
				assert.equal(details.args[0], 101, 'should add the args');
				assert.equal(details.params.foo, 'bar', 'should add the params to the details');
				assert(details.timing >= 0, 'should have timed the invocation');
				done();
			}
			catch (e) {
				done(e);
			}
		});

		instance.registerAction(actionStub);
	});

	it('should be able to subscribe to errors when actions reject', (done) => {
		// Make the action reject.
		actionStub.invoke = () => Promise.reject(new Error('the-error'));

		// Need to reach into the router stub to be able to invoke the callback.
		routerStub.onSubscribe.subscribe((details: any) => {
			// Invoke the callback
			details.callback(0, [101], { foo: 'bar' });
		});

		instance.onError.subscribe((details: any) => {
			try {
				assert.equal(details.action, 'foo.bar', 'should be the name of the action');
				assert.equal(details.err.message, 'the-error', 'should be the error message');
				assert.equal(details.authid, 0, 'should add the authid to the details');
				assert.equal(details.args[0], 101, 'should add the args');
				assert.equal(details.params.foo, 'bar', 'should add the params to the details');
				assert(details.timing >= 0, 'should have timed the invocation');
				done();
			}
			catch (e) {
				done(e);
			}
		});

		instance.subscribeAction(actionStub);
	});

	it('should catch an unexpected error when invoking', (done) => {
		// Make the action reject.
		actionStub.invoke = () => {
			const foo: any = void 0;

			return foo['bar'];
		};

		// Need to reach into the router stub to be able to invoke the callback.
		routerStub.onSubscribe.subscribe((details: any) => {
			// Invoke the callback
			details.callback(0, [101], { foo: 'bar' });
		});

		instance.onError.subscribe((details: any) => {
			try {
				assert.equal(details.action, 'foo.bar', 'should be the name of the action');
				assert.equal(details.err.message, 'Cannot read property \'bar\' of undefined', 'should be the error message');
				assert.equal(details.authid, 0, 'should add the authid to the details');
				assert.equal(details.args[0], 101, 'should add the args');
				assert.equal(details.params.foo, 'bar', 'should add the params to the details');
				assert(details.timing >= 0, 'should have timed the invocation');
				done();
			}
			catch (e) {
				done(e);
			}
		});

		instance.subscribeAction(actionStub);
	});

	it('should reject starting a microservice without a router', () => {
		instance.setRouter(void 0);

		return assertRejects(
			instance.start(),
			/Router required/
		);
	});
});
