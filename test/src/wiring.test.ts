/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Sandal = require('sandal');
import * as assert from 'assert';
import {DiContainer} from 'dicontainer';
import {Module} from '../../src/interfaces/Module';
import {register, start} from '../../src/index';

describe('wiring unit tests', () => {
	describe('register', () => {
		it('should register `defaultConfig` if not in the container', (done) => {
			const container: DiContainer = new Sandal();

			register(container, []);

			assert(container.has('defaultConfig'), 'should add `defaultConfig`');

			container.resolve((err: Error, defaultConfig: any) => {
				if (err) {
					return done(err);
				}

				assert.deepEqual(defaultConfig, {}, 'should be an empty object');
				done();
			});
		});

		it('should register `defaultConfig` via injection', (done) => {
			const container: DiContainer = new Sandal();

			register(container, [], { foo: 'bar' });

			assert(container.has('defaultConfig'), 'should add `defaultConfig`');

			container.resolve((err: Error, defaultConfig: any) => {
				if (err) {
					return done(err);
				}

				assert.deepEqual(defaultConfig.foo, 'bar', 'should be the injected config');
				done();
			});
		});

		it('should register modules', () => {
			const container: DiContainer = new Sandal();
			const module: Module = {
				register: (container: DiContainer) => {
					container.object('testing', true);
				}
			};

			register(container, [module]);

			assert(container.has('testing'), 'should have registered the module in the container');
		});
	});

	describe('start', () => {
		it('should execute events in order', () => {
			const container: DiContainer = new Sandal();
			const steps: string[] = [];
			const module1: Module = {
				register: () => {},
				preConfigureFirst: () => steps.push('preConfigureFirst'),
				preConfigure: () => steps.push('preConfigure'),
				configure: () => steps.push('configure')
			};
			const module2: Module = {
				register: () => {},
				init: () => steps.push('init'),
				start: () => steps.push('start')
			};

			return start(container, [module1, module2])
				.then(() => {
					assert.deepEqual(steps, [
						'preConfigureFirst',
						'preConfigure',
						'configure',
						'init',
						'start'
					]);
				});
		});
	});
});
