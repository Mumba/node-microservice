/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {DeleteDataAction, DeletingDataService} from '../../../src/index';

describe('DeleteDataAction unit tests', () => {
	let dataService: DeletingDataService<any>;
	let instance: DeleteDataAction<any>;
	const authId = 101;
	const args = [1];
	const kwargs = {};

	beforeEach(() => {
		dataService = {
			delete: (id: number) => Promise.resolve({})
		};
		instance = new DeleteDataAction(dataService, 'data.delete');
	});

	it('should invoke', () => {
		dataService.delete = (id: number) => new Promise((resolve) => {
			assert.equal(id, args[0], 'should be the id to delete');
			resolve({});
		});

		return instance.invoke(authId, args, kwargs);
	});

	it('should call afterDeleteDataItem after adding data', () => {
		class ConcreteDeleteDataAction extends DeleteDataAction<any> {
			protected afterDeleteDataItem(values: any) {
				values.spy = true;
			}
		}

		const concrete = new ConcreteDeleteDataAction(dataService, 'concrete');

		return concrete.invoke(authId, args, kwargs)
			.then((results) => {
				assert.strictEqual(results[0].spy, true, 'should have called hook');
			});
	});
});
