/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {CountDataAction, CountingDataService} from '../../../src/index';

describe('CountDataAction unit tests', () => {
	let dataService: CountingDataService;
	let instance: CountDataAction;
	const authId = 101;
	const count = 5;

	beforeEach(() => {
		dataService = {
			count: (options: any) => Promise.resolve(count)
		};
		instance = new CountDataAction(dataService, 'data.count');
	});

	it('should invoke with filters', () => {
		const args = [1];
		const kwargs = { foo: 99 };
		const expected = 5;

		dataService.count = (options: any) => new Promise((resolve) => {
			assert.deepEqual(options, {}, 'should be the options');
			resolve(expected);
		});

		return instance.invoke(authId, args, kwargs)
			.then((result) => {
				assert.equal(result, expected, 'should be the stubbed result');
			});
	});
});
