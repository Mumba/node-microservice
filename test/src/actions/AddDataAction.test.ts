/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {AddDataAction, AddingDataService} from '../../../src/index';

describe('AddDataAction unit tests', () => {
	let dataService: AddingDataService<any>;
	let instance: AddDataAction<any>;

	beforeEach(() => {
		dataService = {
			add: (values: any) => Promise.resolve(values)
		};
		instance = new AddDataAction(dataService, 'data.add');
	});

	it('should invoke', () => {
		const authId = 101;
		const args = [{ foo: 'bar' }];
		const kwargs = {};

		return instance.invoke(authId, args, kwargs)
			.then((results) => {
				assert.equal(results[0].foo, 'bar', 'should be the add result');
			});
	});

	it('should call afterAddDataItem after adding data', () => {
		const authId = 101;
		const args = [{ foo: 'bar' }];
		const kwargs = {};

		class ConcreteAddDataAction extends AddDataAction<any> {
			protected afterAddDataItem(values: any) {
				values.spy = true;
			}
		}

		const concrete = new ConcreteAddDataAction(dataService, 'concrete');

		return concrete.invoke(authId, args, kwargs)
			.then((results) => {
				assert.strictEqual(results[0].spy, true, 'should have called hook');
			});
	});
});
