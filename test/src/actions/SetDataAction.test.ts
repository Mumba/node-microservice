/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {SetDataAction, SettingDataService} from '../../../src/index';

describe('SetDataAction unit tests', () => {
	let dataService: SettingDataService<any>;
	let instance: SetDataAction<any>;
	const authId = 101;
	const args = [{ foo: 'bar' }];
	const kwargs = {};

	beforeEach(() => {
		dataService = {
			set: (values: any) => Promise.resolve(values)
		};
		instance = new SetDataAction(dataService, 'data.set');
	});

	it('should invoke', () => {
		return instance.invoke(authId, args, kwargs)
			.then((results) => {
				assert.equal(results[0].foo, 'bar', 'should be the add result');
			});
	});

	it('should call afterSetDataItem after adding data', () => {
		class ConcreteSetDataAction extends SetDataAction<any> {
			protected afterSetDataItem(values: any) {
				values.spy = true;
			}
		}

		const concrete = new ConcreteSetDataAction(dataService, 'concrete');

		return concrete.invoke(authId, args, kwargs)
			.then((results) => {
				assert.strictEqual(results[0].spy, true, 'should have called hook');
			});
	});
});
