/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {GetDataAction, GettingDataService} from '../../../src/index';

describe('GetDataAction unit tests', () => {
	let dataService: GettingDataService<any>;
	let instance: GetDataAction<any>;
	const authId = 101;

	beforeEach(() => {
		dataService = {
			get: (options: any) => Promise.resolve([])
		};
		instance = new GetDataAction(dataService, 'data.add');
	});

	it('should invoke without options', () => {
		const args: any[] = [];
		const kwargs = {};

		return instance.invoke(authId, args, kwargs)
			.then((results) => {
				assert(Array.isArray(results), 'should be the stubbed result');
			});
	});

	it('should invoke with `ids` filter, setting limit to the args length', () => {
		const args = [1, 2, 3];
		const kwargs = { limit: 99 };

		dataService.get = (options: any) => new Promise((resolve) => {
			assert.deepEqual(options.ids, args, 'should be the ids filter');
			assert.equal(options.limit, args.length, 'should set the limit if args is set');
			resolve();
		});

		return instance.invoke(authId, args, kwargs);
	});

	it('should invoke with `limit`', () => {
		const args: any[] = [];
		const kwargs = { limit: 5 };

		dataService.get = (options: any) => new Promise((resolve) => {
			assert.equal(options.limit, kwargs.limit, 'should set the limit');
			resolve();
		});

		return instance.invoke(authId, args, kwargs);
	});

	it('should invoke with `offset`', () => {
		const args: any[] = [];
		const kwargs = { offset: 4 };

		dataService.get = (options: any) => new Promise((resolve) => {
			assert.equal(options.offset, kwargs.offset, 'should set the offset');
			resolve();
		});

		return instance.invoke(authId, args, kwargs);
	});

	it('should invoke with a default `limit` and `offset', () => {
		const args: any[] = [];
		const kwargs = {};

		dataService.get = (options: any) => new Promise((resolve) => {
			assert.equal(options.limit, GetDataAction.DEFAULT_LIMIT, 'should set the default limit');
			assert.equal(options.offset, 0, 'should set the default offset');
			resolve();
		});

		return instance.invoke(authId, args, kwargs);
	});
});
