/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {AbstractDataAction} from '../../../src/index';

class ConcreteDataAction extends AbstractDataAction<any> {
	public invoke() {
		return Promise.resolve([]);
	}
}

describe('AbstractDataAction unit tests', () => {
	let dataService: any;
	let instance: ConcreteDataAction;

	beforeEach(() => {
		dataService = {};
		instance = new ConcreteDataAction(dataService, 'action.name');
	});

	it('should throw if `dataService` missing in constructor', () => {
		assert.throws(() => {
			instance = new ConcreteDataAction(void 0, 'action.name');
		}, /<dataService> required/);
	});

	it('should set the action name via constructor', () => {
		assert.equal(instance.getName(), 'action.name', 'should be the name');
	});

	it('should set the action name via method', () => {
		assert.strictEqual(instance.setName('new-name'), instance, 'should chain');
		assert.equal(instance.getName(), 'new-name', 'should be the name');
	});

	it('should get the dataService using the constructor', () => {
		assert.strictEqual(instance.getDataService(), dataService, 'should be the name');
	});

	it('should set the dataService via method', () => {
		const newDataService = {};

		assert.strictEqual(instance.setDataService(newDataService), instance, 'should chain');
		assert.strictEqual(instance.getDataService(), newDataService, 'should be the dataService');
	});
});
