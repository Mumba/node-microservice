/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {DiContainer} from 'dicontainer';

export interface Module {
	register(container: DiContainer): void;

	preConfigureFirst?(...args: any[]): any;

	preConfigure?(...args: any[]): any;

	configure?(...args: any[]): any;

	init?(...args: any[]): any;

	start?(...args: any[]): any;
}
