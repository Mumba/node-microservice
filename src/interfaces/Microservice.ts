/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Subject} from 'rxjs/Subject';
import {Router} from 'mumba-microservice-router';
import {Action} from './Action';

export interface Microservice {
	onRegister: Subject<any>;
	onSubscribe: Subject<any>;
	onInvoke: Subject<any>;

	/**
	 * Set the router object.
	 *
	 * @param {Router} router
	 * @returns {Microservice}
	 */
	setRouter(router: Router): this;

	/**
	 * Register an action against the router.
	 *
	 * @param {Action} action
	 * @returns this
	 */
	registerAction(action: Action): this;

	/**
	 * Subscribe an action to a topic on the router.
	 *
	 * @param {Action} action
	 * @returns this
	 */
	subscribeAction(action: Action): this;

	/**
	 * Start the Microservice.
	 */
	start(): Promise<void>;
}
