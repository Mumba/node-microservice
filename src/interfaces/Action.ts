/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

export interface Action {
	/**
	 * The name of the action.
	 *
	 * @return {string}
	 */
	getName(): string;

	/**
	 * Invokes the action, this is where the main logic for the action is performed.
	 *
	 * - An example of a WAMP action could be a RPC method that takes args and fetches
	 *   a list of Users from the database and then returns them.
	 *
	 * @param {number} authId - The ID of the authorised caller.
	 * @param {array   args   - An array of arguments.
	 * @param {any}    params - A dictionary of arguments by which the action is invoked.
	 * @returns {Promise<any>} - The result of the action
	 */
	invoke(authId: number, args: any[], params: any): Promise<any>;
}
