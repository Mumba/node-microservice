/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {AbstractDataAction} from './AbstractDataAction';

export interface CountingDataService {
	count(options: any): Promise<number>;
}

export class CountDataAction extends AbstractDataAction<CountingDataService> {
	/**
	 * Invokes the action.
	 */
	public invoke(authId: number, args: number[], kwargs: any = {}): Promise<number> {
		return this.getDataService()
			.count(this.getCountOptions(args, kwargs));
	}

	/**
	 * Get the search criteria for counting data.
	 */
	protected getCountOptions(args: any[] = [], kwargs: any = {}): any {
		return {};
	}
}
