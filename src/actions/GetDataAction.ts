/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {AbstractDataAction} from './AbstractDataAction';

export interface GettingDataService<A> {
	get(options: any): Promise<A[]>;
}

export class GetDataAction<A>  extends AbstractDataAction<GettingDataService<A>> {
	public static DEFAULT_LIMIT = 10;

	/**
	 * Invokes the action.
	 */
	public invoke(authId: number, args: number[], kwargs: any = {}): Promise<A[]> {
		return this.getDataService()
			.get(this.getFindAllOptions(args, kwargs));
	}

	/**
	 * Get the search criteria for finding data.
	 *
	 * Automatically sets:
	 * - `.ids` from the `args`
	 * - `.limit` from `kwargs.limit`
	 * - `.offset` from `kwargs.limit`
	 *
	 * Note that if `args` are passed, the `limit` is set to the number of args.
	 * Getting data by ID cannot be paginated.
	 */
	protected getFindAllOptions(args: any[] = [], kwargs: any = {}): any {
		return {
			ids: args.length > 0 ? args : void 0,
			limit: args.length || +kwargs.limit || GetDataAction.DEFAULT_LIMIT,
			offset: +kwargs.offset || 0
		};
	}
}
