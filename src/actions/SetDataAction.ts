/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {AbstractDataAction} from './AbstractDataAction';

export interface SettingDataService<A> {
	set(values: A): Promise<A>;
}

/**
 * Action for setting existing data in a data source
 */
export class SetDataAction<A> extends AbstractDataAction<SettingDataService<A>> {
	/**
	 * Invokes the action.
	 */
	public invoke(authId: number, args: A[], kwargs: any = {}): Promise<any[]> {
		return Promise.all(
			args.map((arg: A) => this.setDataItem(arg)
				.then((result: A) => {
					this.afterSetDataItem(result);

					return result;
				})
			)
		);
	}

	protected setDataItem(values: A) {
		return this.getDataService()
			.set(values);
	}

	/**
	 * Post-set hook.
	 *
	 * This method is called after the data is set with the result of `dataService.set`.
	 */
	protected afterSetDataItem(values: A) {
	}
}
