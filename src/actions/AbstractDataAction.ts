/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Action} from '../interfaces/Action';

/**
 * Abstract data action (for CRUD).
 */
export abstract class AbstractDataAction<DS> implements Action {
	private name: string;

	/**
	 * @param {any}    dataService - A data service.
	 * @param {string} name        - The name of the action (usually the routing key).
	 */
	constructor(private dataService: DS, name: string) {
		if (!dataService) {
			throw new Error('AbstractDataAction: <dataService> required.');
		}

		this.setName(name);
	}

	/**
	 * Set the name of the action.
	 *
	 * @param {string} name
	 * @returns {AbstractDataAction}
	 */
	public setName(name: string): this {
		this.name = name;

		return this;
	}

	/**
	 * Get the name of the action.
	 *
	 * @returns {string}
	 */
	public getName(): string {
		return this.name;
	}

	/**
	 * Set the data service.
	 *
	 * @param {any} dataService
	 * @returns {AbstractDataAction}
	 */
	public setDataService(dataService: DS) {
		this.dataService = dataService;

		return this;
	}

	/**
	 * Get the data service.
	 *
	 * @returns {DS}
	 */
	public getDataService(): DS {
		return this.dataService;
	}

	/**
	 * Invoke the action.
	 *
	 * @param {number} authId - The ID of the authenticated user.
	 * @param {*[]}    args
	 * @param {*}      kwargs
	 */
	public abstract invoke(authId: number, args: any[], kwargs: any): Promise<any>;
}
