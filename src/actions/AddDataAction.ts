/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {AbstractDataAction} from './AbstractDataAction';

export interface AddingDataService<A> {
	add(values: A): Promise<A>;
}

/**
 * Action for adding data to a data source
 */
export class AddDataAction<A> extends AbstractDataAction<AddingDataService<A>> {
	/**
	 * Invokes the action.
	 */
	public invoke(authId: number, args: A[], kwargs: any = {}): Promise<any[]> {
		return Promise.all(
			args.map((arg: A) => this.addDataItem(arg)
				.then((result: A) => {
					this.afterAddDataItem(result);

					return result;
				})
			)
		);
	}

	protected addDataItem(values: A) {
		return this.getDataService()
			.add(values);
	}

	/**
	 * Post-add hook.
	 *
	 * This method is called after the data is added with the result of `dataService.add`.
	 */
	protected afterAddDataItem(values: A) {
	}
}
