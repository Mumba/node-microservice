/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {AbstractDataAction} from './AbstractDataAction';

export interface DeletingDataService<A> {
	delete(id: number): Promise<A>;
}

/**
 * Action for deleting data in a data source
 */
export class DeleteDataAction<A> extends AbstractDataAction<DeletingDataService<A>> {
	/**
	 * Invokes the action.
	 */
	public invoke(authId: number, args: number[], kwargs: any = {}): Promise<any[]> {
		return Promise.all(
			args.map((arg: number) => this.getDataService().delete(arg)
				.then((result) => {
					this.afterDeleteDataItem(result);

					return result;
				})
			)
		);
	}

	/**
	 * Post-delete hook.
	 *
	 * This method is called after the data is deleted with the result of `dataService.delete`.
	 * It is passed a copy of the object that was deleted.
	 */
	protected afterDeleteDataItem(values: A) {
	}
}
