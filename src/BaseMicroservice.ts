/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Subject} from 'rxjs/Subject';
import {Router} from 'mumba-microservice-router';
import {Action} from './interfaces/Action';
import {Microservice} from './interfaces/Microservice';

/**
 * Schema for the Microservice constructor options.
 */
export interface BaseMicroserviceOptions {
}

/**
 * Base implementation of a Microservice interface.
 */
export class BaseMicroservice implements Microservice {
	public onRegister: Subject<any> = new Subject();
	public onSubscribe: Subject<any> = new Subject();
	public onInvoke: Subject<any> = new Subject();
	public onError: Subject<any> = new Subject();

	private options: BaseMicroserviceOptions;
	private router: Router;

	public constructor(microserviceOptions: BaseMicroserviceOptions, router: Router) {
		if (!router) {
			throw new Error('BaseMicroservice: <router> required');
		}

		this.setRouter(router);
		this.options = microserviceOptions;

		// TODO - This is tightly coupled to the response of the Wamp Router. May need to fix this one day.
		this.router.onRegister.subscribe(
			(res: any) => this.onRegister.next(res.procedure),
			(err: Error) => this.onError.next(err)
		);

		this.router.onSubscribe.subscribe(
			(res: any) => this.onSubscribe.next(res.topic),
			(err: Error) => this.onError.next(err)
		);
	}

	/**
	 * Set the router object.
	 */
	public setRouter(router: Router): this {
		this.router = router;

		return this;
	}

	/**
	 * Register an action against the router.
	 */
	public registerAction(action: Action): this {
		this.router.register(action.getName(), (authid: number, args: any[] = [], params: any = {}) => {
			return this.invoke(action, authid, args, params);
		});

		return this;
	}

	/**
	 * Subscribe an action to a topic on the router.
	 */
	public subscribeAction(action: Action) {
		this.router.subscribe(action.getName(), (authid: number, args: any[] = [], params: any = {}) => {
			return this.invoke(action, authid, args, params);
		});

		return this;
	}

	/**
	 * Skeleton start routine.
	 */
	public start(): Promise<void> {
		if (!this.router) {
			return Promise.reject(new Error('BaseMicroservice.start: Router required to start microservice.'));
		}

		return Promise.resolve();
	}

	/**
	 * Invocation wrapper for an action.
	 */
	protected invoke(action: Action, authid: number, args: any[], kwargs: any) {
		const start = Date.now();

		return new Promise((resolve) => {
			resolve(action.invoke(authid, args, kwargs)
				.then((response: any) => {
					this.onInvoke.next({
						action: action.getName(),
						authid,
						args,
						params: kwargs,
						timing: Date.now() - start
					});

					return response;
				}));
		})
			.catch((err: Error) => {
				this.onError.next({
					err,
					action: action.getName(),
					authid,
					args,
					params: kwargs,
					timing: Date.now() - start
				});

				throw err instanceof Error ? err.message : err;
			});
	}
}
