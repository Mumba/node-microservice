/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {DiContainer} from 'dicontainer';
import {Module} from './interfaces/Module';

/**
 * Register modules with the DI container.
 *
 * @param {DiContainer} container
 * @param {Module[]}    modules
 * @param {object}      defaultConfig
 */
export function register(container: DiContainer, modules: Module[], defaultConfig: any = {}) {
	if (!container.has('defaultConfig')) {
		container.object('defaultConfig', defaultConfig);
	}

	modules.forEach((module: any) => module.register(container));
}

/**
 * Start the microservice life-cycle.
 *
 * @param {DiContainer} container
 * @param {object[]}    modules
 * @returns {Promise<any[]>}
 */
export function start(container: DiContainer, modules: Module[]) {
	function doStep(method: string) {
		const promises: any[] = modules
			.filter((module: any) => module[method] !== void 0)
			.map((module: any) => {
				return new Promise((resolve, reject) => {
					container.resolveAsFactory(module[method], (err: Error, output: any) => {
						return err ? reject(err) : resolve(output);
					});
				});
			});

		return Promise.all(promises);
	}

	return doStep('preConfigureFirst')
		.then(() => doStep('preConfigure'))
		.then(() => doStep('configure'))
		.then(() => doStep('init'))
		.then(() => doStep('start'));
}
