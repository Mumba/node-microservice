# Mumba Microservice

## Description

Microservice definitions.

## Installation 

```sh
$ npm install --save mumba-microservice
```

## Example

TODO

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## People

The original author of _Mumba WAMP_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-microservice/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2017 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

