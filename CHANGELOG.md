# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.3.10] 23 Oct 2017
- Updated deps; refactored `AddDataAction` and `SetDataAction` to be a bit more extendable.

## [0.3.9] 9 Aug 2017
- Added `CountDataAction`.

## [0.3.8] 26 Jul 2017
- Added missing export for `DeleteDataAction`.

## [0.3.7] 26 Jul 2017
- Added linting.
- Added `DeleteDataAction`.

## [0.3.6] 10 Jul 2017
- Added `rxjs` to `package.json`.

## [0.3.5] 29 May 2017
- Updated deps (update to Autobahn).

## [0.3.4] 17 May 2017
- Removed max limit on get results.

## [0.3.3] 10 Feb 2017
- Relaxed return type on `AbstractDataAction.invoke` to `any`.

## [0.3.1,0.3.2] 9 Feb 2017
- Added `AbstractDataAction`, `AddDataAction`, `GetDataAction` and `SetDataAction` as standard CRUD blueprints.

## [0.3.0] 16 Nov 2016
- Updated deps.
- Added `onError` observer.

## [0.2.0] 20 Oct 2016
- Added `register` and `start` wiring methods.

## [0.1.2] 5 Aug 2016
- Added exports for DiContainer.

## [0.1.1] 1 Aug 2016
- Improve error catching when invoking an action.

## [0.1.0] 21 Jul 2016
- Initial release
